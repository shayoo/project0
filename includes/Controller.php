<?php
class Controller{

	public function __construct(){
	
	}

	function view($template,$data){
		require_once('includes/helper.php');
		render('templates/header',array("title"=>$data));
		render($template);
		render('templates/footer');
		
		
	}

	function model($loadmodel)
	{
		require_once('models/'.$loadmodel.'_model.php');

	}
	

}




?>
