<?php
class bootstrap{
	function __construct(){
		if(isset($_GET['url'])){
		$check=htmlspecialchars($_GET['url']);
		$url=urlencode($check);
		$url = rtrim($url, '/');
		$url=explode('/',$check);
		$path='controllers/'.$url[0].'.php';
		if(file_exists($path)){
			require 'controllers/'.$url[0].'.php';
			$controller=new $url[0];
			$controller->view($url[0],$url[0]);
		}
		else{
			$this->error();
		}
		}
		else{
		require 'controllers/index.php';
		$controller=new index();
		$controller->view("index","index");
		}
		//method calling
		if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                $this->error();
            }
        } 
        else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $this->error();
                }
            } else {
                
            }
        }

	}

	function error(){
	require 'controllers/error.php';
	$controller=new error();
	$controller->view("error","404 Error!");
	return false;
	}
}
?>
